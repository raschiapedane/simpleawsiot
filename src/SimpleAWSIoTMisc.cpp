/*
SimpleAWSIoT Library. A simple library to connect to AWS IoT Core
Copyright (C) 2021  Andrea Prosperi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <SimpleAWSIoTMisc.h>

#if defined(ESP8266)
#include <sys/time.h>
#endif

#define TZ              1      // (utc+) TZ in hours
#define DST_MN          0      // use 60mn for summer time in some countries
#define TZ_MN           ((TZ)*60)
#define TZ_SEC          ((TZ)*3600)
#define DST_SEC         ((DST_MN)*60)

// Set time via NTP, as required for x.509 validation
time_t setClock() {
  configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");

  Serial.print(F("setClock - Waiting for NTP time sync: "));
  time_t now = time(nullptr);
  while (now < 8 * 3600 * 2) {
    delay(500);
    Serial.print(F("."));
    now = time(nullptr);
  }
  Serial.println(F(""));
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.print(F("Current time: "));
  Serial.print(asctime(&timeinfo));
  
  timeval tv = { now, 0 };
  timezone tz = { TZ_MN + DST_MN, 0 };
  settimeofday(&tv, &tz);
  return now;
}

void readFileContent(const char * fileName, char **buffer) {
  #if defined(ESP8266)
  LittleFS.begin();
  File file =  LittleFS.open(fileName, "r");
  #endif

  #if defined(ESP32)
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
  }
  File file =  SPIFFS.open(fileName, FILE_READ);
  #endif

  const size_t file_size = file.size();
  file.readBytes(*buffer, file_size);
  file.close();
}

void readStoredConfiguration(JsonDocument& document) {
  Serial.println(F("readStoredConfiguration - Reading stored configuration"));

  #if defined(ESP8266)
  LittleFS.begin();
  File file =  LittleFS.open(CONFIG_FILE, "r");
  #endif

  #if defined(ESP32)
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
  }
  File file =  SPIFFS.open(CONFIG_FILE, FILE_READ);
  #endif    

  DeserializationError error = deserializeJson(document, file);
  file.close();
  
  // Test if parsing succeeds.
  if (error) {
      Serial.print(F("readStoredConfiguration - deserializeJson() failed: "));
      Serial.println(error.f_str());
      return;
  }
}