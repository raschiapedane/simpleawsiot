/*
SimpleAWSIoT Library. A simple library to connect to AWS IoT Core
Copyright (C) 2021  Andrea Prosperi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <SimpleAWSIoTCore.h>

const int numberOfShadowsTopic = 10;
MQTTClient _mqtt = MQTTClient(PAYLOAD_SIZE);
WiFiClientSecure _client;

SimpleAWSIoT::SimpleAWSIoT() {
}

void SimpleAWSIoT::_setConfiguration(const JsonDocument &config) {
      if (config.containsKey(CONFIG_KEY_SID)) {
        auto value = config[CONFIG_KEY_SID].as<const char *>();
        Serial.printf("SimpleAWSIoT::setConfiguration - Setting %s to %s\n", CONFIG_KEY_SID, value);
        strlcpy(configuration.ssid, value, CONFIG_VALUE_SIZE);
    }

    if (config.containsKey(CONFIG_KEY_PWD)) {
        auto value = config[CONFIG_KEY_PWD].as<const char *>();
        Serial.printf("SimpleAWSIoT::setConfiguration - Setting password to ***********\n");
        strlcpy(configuration.pwd, value, CONFIG_VALUE_SIZE);
    }

    if (config.containsKey(CONFIG_KEY_AWSEP)) {
        auto value = config[CONFIG_KEY_AWSEP].as<const char *>();
        Serial.printf("SimpleAWSIoT::setConfiguration - Setting %s to %s\n", CONFIG_KEY_AWSEP, value);
        strlcpy(configuration.awsep, value, CONFIG_VALUE_SIZE);
    }

    if (config.containsKey(CONFIG_KEY_THINGNAME)) {
        auto value = config[CONFIG_KEY_THINGNAME].as<const char *>();
        Serial.printf("SimpleAWSIoT::setConfiguration - Setting %s to %s\n", CONFIG_KEY_THINGNAME, value);
        strlcpy(configuration.thingName, value, CONFIG_VALUE_SIZE);
    }

    if (config.containsKey(CONFIG_KEY_THINGID)) {
        auto value = config[CONFIG_KEY_THINGID].as<const char *>();
        Serial.printf("SimpleAWSIoT::setConfiguration - Setting %s to %s\n", CONFIG_KEY_THINGID, value);
        strlcpy(configuration.thingId, value, CONFIG_VALUE_SIZE);
    }

    if (config.containsKey(CONFIG_KEY_TOPIC_PREFIX)) {
        auto value = config[CONFIG_KEY_TOPIC_PREFIX].as<const char *>();
        Serial.printf("SimpleAWSIoT::setConfiguration - Setting %s to %s\n", CONFIG_KEY_TOPIC_PREFIX, value);
        strlcpy(configuration.topicPrefix, value, CONFIG_VALUE_SIZE);
    }
}

void SimpleAWSIoT::begin(const JsonDocument &config, MQTTClientCallbackAdvancedFunction callback) {
  //Start networking systems, filesystem, connect to the cloud
  _setConfiguration(config);
  _setupWifi(configuration.ssid, configuration.pwd);
  setClock();
  _setupSecureClient();

  _mqtt.begin(configuration.awsep, 8883, _client);
  _mqtt.onMessageAdvanced(callback);
  //set keep alive to 120 seconds
  _mqtt.setOptions(120, true, 1000);
}

void SimpleAWSIoT::_setupWifi(const char *ssid, const char *passwd) {
  Serial.printf("SimpleAWSIoT::_setupWifi - Connecting to SSID %s", ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);

  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }

  Serial.println(F("WiFi connected"));
  Serial.print(F("SimpleAWSIoT::_setupWifi - IP address: "));
  Serial.println(WiFi.localIP());
}

void SimpleAWSIoT::_setupSecureClient() {
  const char *amazonCAFileName = "/AmazonCA.pem";
  char certFileName[50]; sprintf(certFileName, "/%s.crt", configuration.thingId);
  char privateKeyName[50]; sprintf(privateKeyName, "/%s.key", configuration.thingId);

  #if defined(ESP8266)
  char *rootCA = (char *) calloc(1, CERTSIZE); 
  char *certificate = (char *) calloc(1, CERTSIZE); 
  char *privateKey = (char *) calloc(1, CERTSIZE); 

  readFileContent(amazonCAFileName, &rootCA); 
  readFileContent(certFileName, &certificate);
  readFileContent(privateKeyName, &privateKey); 
  
  //Serial.println(rootCA);
  //Serial.println(certificate);
  //Serial.println(privateKey);

  BearSSL::X509List *caCert = new X509List(rootCA);
  _client.setTrustAnchors(caCert);
  
  BearSSL::X509List *clientCert = new X509List(certificate);
  BearSSL::PrivateKey *key = new PrivateKey(privateKey);
  _client.setClientRSACert(clientCert, key);
  #endif

  #if defined(ESP32)
  SPIFFS.begin();
  File file =  SPIFFS.open(amazonCAFileName, FILE_READ);
  _client.loadCACert(file, file.size());
  file.close();

  file =  SPIFFS.open(certFileName, FILE_READ);
  _client.loadCertificate(file, file.size());
  file.close();

  file =  SPIFFS.open(privateKeyName, FILE_READ);
  _client.loadPrivateKey(file, file.size());
  file.close();
  #endif
}

bool SimpleAWSIoT::connected() {
  return _mqtt.connected();
}

void SimpleAWSIoT::connect() {
  if (!_mqtt.connected()) {
    Serial.printf("SimpleAWSIoT::connect - Connecting to AWS IoT endpoint %s - thing name: %s\n", configuration.awsep, configuration.thingName);
    
    if (!_mqtt.connect(configuration.thingName)) {
      char *errorMsg = (char *)calloc(1, ERROR_MSG_SIZE);
      int lastError = _mqtt.lastError();
      if (lastError != LWMQTT_SUCCESS) {
        Serial.printf("SimpleAWSIoT::connect - MQTT last error %d\n", _mqtt.lastError());
      }

      #if defined(ESP8266)
      lastError = _client.getLastSSLError(errorMsg, ERROR_MSG_SIZE);
      if (lastError != 0) {
        Serial.printf("SimpleAWSIoT::connect - Client LastSSLError %d: %s\n", lastError, errorMsg);
      }
      free(errorMsg);
      #endif
    }
  }
}

bool SimpleAWSIoT::publish(const char *topic, const char *payload) {
  bool retcode = false;
  if (_mqtt.connected()) {
    Serial.printf("SimpleAWSIoT::publish - Sending message to AWS. Topic: %s, Payload %s\n", topic, payload);
    if (_mqtt.publish(topic, payload)) {
      retcode = true;
    } else {
      Serial.printf("SimpleAWSIoT::publish - Error publishing to AWS: Error %d\n", _mqtt.lastError());
    }
  } else {
    Serial.println(F("SimpleAWSIoT::publish - Unable to publish. MQTT not connected"));
  }
  return retcode;
}

void SimpleAWSIoT::loop() {
  _mqtt.loop();
}

bool SimpleAWSIoT::subscribe(const char *topic) {
  bool retcode = false;
  if (_mqtt.connected() == false) {
    Serial.println("SimpleAWSIoT::subscribe - MQTT not connected. Can't subscribe");
    return false;
  }
  if (_mqtt.subscribe(topic)) {
    retcode = true;
  } else {
    Serial.printf("SimpleAWSIoT::subscribe - Error subscribing to topic %s: Error %d\n", topic, _mqtt.lastError());
  }
  return retcode;
}

bool SimpleAWSIoT::subscribeShadows(ShadowTopics topicsToSubScribe[], int numTopics) {
  bool retcode = false;
  if (_mqtt.connected() == false) {
    Serial.println("SimpleAWSIoT::subscribeShadow - MQTT not connected. Can't setup shadow");
    return false;
  }

  for (int i = 0; i < numTopics; i++) {
    char topic[TOPIC_SIZE];
    shadowTopicFromId(topicsToSubScribe[i], topic);
    Serial.printf("SimpleAWSIoT::subscribeShadow - Subscribing to topic %s\n", topic);
    if (_mqtt.subscribe(topic)) {
      retcode = true;
    } else {
      Serial.printf("SimpleAWSIoT::subscribeShadow - Error subscribing to topic %s: Error %d\n", topic, _mqtt.lastError());
    }
  }
  return retcode;
}

void SimpleAWSIoT::getShadow() {
  if (_mqtt.connected()) {
    char shadowTopics[TOPIC_SIZE];
    snprintf(shadowTopics, TOPIC_SIZE, "$aws/things/%s/shadow/get", configuration.thingName);
    Serial.printf("SimpleAWSIoT::getShadow - Asking shadow using topic %s\n", shadowTopics);
    publish(shadowTopics, "");
  } else {
    Serial.println(F("SimpleAWSIoT::getShadow - MQTT Not connected"));
  }
}

void SimpleAWSIoT::shadowTopicFromId(ShadowTopics topicId, char *topic) {
    switch(topicId) {
      case ShadowTopicGetAccepted:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/get/accepted", configuration.thingName);
        break;
      case ShadowTopicGetRejected:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/get/rejected", configuration.thingName);
        break;
      case ShadowTopicDeleteAccepted:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/delete/accepted", configuration.thingName);
        break;
      case ShadowTopicDeleteRejected:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/delete/rejected", configuration.thingName);
        break;
      case ShadowTopicUpdateAccepted:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/update/accepted", configuration.thingName);
        break;
      case ShadowTopicUpdateRejected:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/update/rejected", configuration.thingName);
        break;
      case ShadowTopicUpdateDelta:  
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/update/delta", configuration.thingName);
        break;
      case ShadowTopicUpdateDocument:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/update/documents", configuration.thingName);
        break;
      case ShadowTopicGet:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/get", configuration.thingName);
        break;
      case ShadowTopicDelete:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/delete", configuration.thingName);
        break;
      case ShadowTopicUpdate:
        snprintf(topic, TOPIC_SIZE, "$aws/things/%s/shadow/update", configuration.thingName);
        break;
      default:
        snprintf(topic, TOPIC_SIZE, "NullTopic");
        break;
    }
}

ShadowTopics SimpleAWSIoT::shadowTopicId(const char *topic) {
  char shadowTopic[TOPIC_SIZE];
  ShadowTopics retcode = NoShadowTopic;

  if (strlen(topic) > 0) {
    for (int i = 0; i < numberOfShadowsTopic; i++) {
      shadowTopicFromId((ShadowTopics)i, shadowTopic);
      if (strncmp(topic, shadowTopic, TOPIC_SIZE) == 0) {
        retcode = (ShadowTopics)i;
      }
    }
  }

  return retcode;
}

bool SimpleAWSIoT::updateShadow(ShadowTopics topicId, const char *payload) {
  char topic[TOPIC_SIZE];
  shadowTopicFromId(topicId, topic);
  return publish(topic, payload);
} 

bool SimpleAWSIoT::updateShadowConnectivityStatus() {
  Serial.println(F("SimpleAWSIoT::updateShadowConnectivityStatus - Updating Connected state to true"));
  char *json = (char *)calloc(1, PAYLOAD_SIZE);
  StaticJsonDocument<PAYLOAD_SIZE> _doc;
  _doc[SHADOW_STATE_KEY][SHADOW_REPORTED_KEY][SHADOW_CONNECTED_KEY] = "true";
  serializeJson(_doc, json, PAYLOAD_SIZE);
  bool retcode = updateShadow(ShadowTopicUpdate, json);
  free(json);
  return retcode;
}

void SimpleAWSIoT::setWillAndTestament() {
  char topic[TOPIC_SIZE];
  completeTopic("lastWill", topic);

  char *json = (char *)calloc(1, PAYLOAD_SIZE);
  StaticJsonDocument<PAYLOAD_SIZE> _doc;
  _doc[SHADOW_STATE_KEY][SHADOW_REPORTED_KEY][SHADOW_CONNECTED_KEY] = "false";
  serializeJson(_doc, json, PAYLOAD_SIZE);

  Serial.printf("SimpleAWSIoT::setWillAndTestament - Setting Last Will using topic %s - payload %s\n", topic, json);
  _mqtt.setWill(topic, json);

  free(json);
}

void SimpleAWSIoT::completeTopic(const char *topic, char *newTopic) {
  snprintf(newTopic, TOPIC_SIZE, "%s/%s/%s", configuration.topicPrefix, configuration.thingName, topic);
}
