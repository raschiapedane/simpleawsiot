/* 
SimpleAWSIoT Library. A simple library to connect to AWS IoT Core
Copyright (C) 2021  Andrea Prosperi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <ArduinoJson.h>
#include <SimpleAWSIoT.h>

SimpleAWSIoT simpleAWSIoT; //SimpleAWSIoT object used to connect to AWS

static char lastTopic[TOPIC_SIZE] = ""; //Buffer used to store lastTopic received
static char lastPayload[PAYLOAD_SIZE] = ""; //Buffer used to store lastPayload
unsigned long _shadowVersion = 0; //Keep track of last shadow version processed


void mqttcallback(MQTTClient *client, char *topic, char *bytes, int length); //MQTT Callback definition

void connectToCloud() {
  if (!simpleAWSIoT.connected()) {
    //WILL HAS TO BE CALLED BEFORE CONNECT
    simpleAWSIoT.setWillAndTestament();
    
    simpleAWSIoT.connect();

    //Subscribe to interesting topics
    ShadowTopics topics[] = {ShadowTopicUpdateDelta, ShadowTopicUpdateRejected, ShadowTopicGetAccepted};
    simpleAWSIoT.subscribeShadows(topics, 3);

    //Get Shadow
    simpleAWSIoT.getShadow();    

    //Update Shadow Connectivity Status
    simpleAWSIoT.updateShadowConnectivityStatus();    
  }
}

/**
 * Execute code every N number of seconds
 */
void timedLoop(int numOfSeconds) {
  static time_t loopTimer = time(nullptr); //Temporary timer
  if (numOfSeconds <= 0) {
    Serial.println(F("timedLoop - Number of seconds should be greater than 0"));
  }
  if (time(nullptr) - loopTimer > numOfSeconds) {
    //Timer expired.
    loopTimer = time(nullptr); 

    char topic[TOPIC_SIZE] = "mytopic/data";

    //Check cloud connection is still alive and connect if needed
    connectToCloud();

    //Publish JSON to cloud
    simpleAWSIoT.completeTopic("data", topic);
    simpleAWSIoT.publish(topic, "Hello World");
  }
}

void _parseMessageReceived(ShadowTopics i, const char *topic, const JsonDocument &cloudMessage) {    
    switch (i) {
      case ShadowTopicGetAccepted:
        //DO SOMETHING
        break;

      case ShadowTopicGetRejected:
        //DO SOMETHING
        break;
      
      case ShadowTopicDeleteAccepted:
        //DO SOMETHING
        break;
      
      case ShadowTopicDeleteRejected:
        //DO SOMETHING
        break;

      case ShadowTopicUpdateAccepted:
        //DO SOMETHING
        break;

      case ShadowTopicUpdateRejected:
        //DO SOMETHING
        break;

      case ShadowTopicUpdateDelta:
        //DO SOMETHING
        break;

      case ShadowTopicUpdateDocument:
        //DO SOMETHING
        break;

      case NoShadowTopic:
        //DO SOMETHING
        break;
        
      default:
        break;
    }
}

unsigned long shadowVersion(const JsonDocument &document) {
  JsonObjectConst root = document.as<JsonObjectConst>();
  unsigned long version = root["version"].as<JsonVariantConst>().as<unsigned long>();  
  Serial.printf("_shadowVersion - Document version %lu\n", version);
  return version;
}

/**
 * MQTT Callback as defined in MQTT.h
 */
void processMQTTMessage(SimpleAWSIoT simpleAWSIoT, const char *topic, const char *payload) {
  DynamicJsonDocument cloudMessage(PAYLOAD_SIZE);

  if (!topic || strlen(topic) == 0) {
    return;
  }

  if (strlen(payload) > 0) {
    DeserializationError error = deserializeJson(cloudMessage, payload);
    if (error) {
        Serial.printf("processLastMQTTMessage - deserializeJson() failed: %s\n", error.c_str());
    }

    //Skip out of order messages looking at version number
    unsigned long lastMessage = shadowVersion(cloudMessage);
    if (lastMessage > _shadowVersion) {
      _shadowVersion = lastMessage;
      _parseMessageReceived(simpleAWSIoT.shadowTopicId(topic), topic, cloudMessage);
    }
  }
}

/**
 * MQTT Callback as defined in MQTT.h
 */
void mqttcallback(MQTTClient *client, char *topic, char *bytes, int length) {
  //Store topic and payload in a global variable and process them into loop
  if (topic) {
    if (strlen(topic) > 0) {
      strlcpy(lastTopic, topic, TOPIC_SIZE);
      Serial.print("Received message on topic: ");
      Serial.println(topic);
    }
  }
  if (bytes) {
    if (strlen(bytes) > 0) {
      strlcpy(lastPayload, bytes, PAYLOAD_SIZE);
        Serial.print("Received payload: ");
        Serial.println(lastPayload);
    }
  }
}

void setup(void) {
  Serial.begin(115200);
  LittleFS.begin();

  //Read Configuration from FLASH
  DynamicJsonDocument configuration(CONFIG_SIZE); //Document storing configuration
  readStoredConfiguration(configuration);

  //Initialize Cloud, connect to AWS and set the mqttcallback function used when message is received from the cloud
  simpleAWSIoT.begin(configuration, mqttcallback);
  connectToCloud();
}

void loop(void) {
  simpleAWSIoT.loop();
  
  //Process lastTopic/lastPayload received by MQTT
  processMQTTMessage(simpleAWSIoT, lastTopic, lastPayload);
  //reset lastTopic, lastPayload after processing
  strlcpy(lastTopic, "", TOPIC_SIZE);
  strlcpy(lastPayload, "", PAYLOAD_SIZE);

  //Call periodic loop every N seconds*/
  timedLoop(10);
}
