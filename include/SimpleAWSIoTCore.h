/*
SimpleAWSIoT Library. A simple library to connect to AWS IoT Core
Copyright (C) 2021  Andrea Prosperi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef __SIMPLEAWSIOT_CORE_H_
#define __SIMPLEAWSIOT_CORE_H_

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#endif

#if defined(ESP32)
#include <Wifi.h>
#endif

#include <MQTTClient.h>
#include <WiFiClientSecure.h>
#include <SimpleAWSIoTMisc.h>
#include <SimpleAWSIoTShadow.h>

/**
 * TOPIC SIZE
*/
#define TOPIC_SIZE 64

/**
 * ERROR MESSAGE SIZE
*/
#define ERROR_MSG_SIZE 64

/**
 * SIZE OF A KEY IN CLOUD CONFIGURATION STRUCT
 */
#define CLOUD_CONFIGURATION_KEY_SIZE 64

/**
 * SIZE OF VALUE ASSOCIATED TO A KEY IN CLOUD CONFIGURATION STRUCT
 */
#define CLOUD_CONFIGURATION_VALUE_SIZE 128

/**
 * SIZE OF A CERTIFICATE FILE
 */
#define CERTSIZE 1800

/**
 * PAYLOAD SIZE OF MESSAGES EXCHANGED WITH AWS.
 * ON ESP8266 SIZE BIGGER THAN 512 BYTES CAUSE RAM OVERFLOW LEADING TO CRASH
 */
#define PAYLOAD_SIZE 512

/**
 * Configuration information to connect to WIFI & AWS Cloud
 */
 struct SimpleAWSIoTConfiguration {
  char ssid[CLOUD_CONFIGURATION_VALUE_SIZE] = "default";            //WIFI SSID
  char pwd[CLOUD_CONFIGURATION_VALUE_SIZE] = "default";             //WIFI PWD
  char awsep[CLOUD_CONFIGURATION_VALUE_SIZE] = "http://127.0.0.1";  //AWS IOT CORE ENDPOINT
  char thingName[CLOUD_CONFIGURATION_VALUE_SIZE] = "default";       //THING NAME as defined in AWS IOT Core
  char thingId[CLOUD_CONFIGURATION_VALUE_SIZE] = "default";         //THIND ID a unique indentifier used to name certificate stored in microflash
  char topicPrefix[CLOUD_CONFIGURATION_VALUE_SIZE] = "default";     //PREFIX applied to topics
};

/**
 * Enumeration of Topics
 */
enum ShadowTopics {
  //DO NOT FORGET THAT THESE ENUMS ARE USED TO DEFINE ARRAY CONSTANT POSITION IN shadowTopicId method
  NoShadowTopic = -1,
  ShadowTopicGetAccepted = 0,
  ShadowTopicGetRejected = 1,
  ShadowTopicDeleteAccepted = 2,
  ShadowTopicDeleteRejected = 3,
  ShadowTopicUpdateAccepted = 4,
  ShadowTopicUpdateRejected = 5,
  ShadowTopicUpdateDelta = 6,
  ShadowTopicUpdateDocument = 7,
  ShadowTopicGet = 8,
  ShadowTopicDelete = 9,
  ShadowTopicUpdate = 10
} typedef ShadowTopics;

class SimpleAWSIoT {
    private:
        void _setupSecureClient();
        void _setupWifi(const char *ssid, const char *passwd);
        void _initShadowTopics();
        void _setConfiguration(const JsonDocument &config);

    public:
        SimpleAWSIoT();
        SimpleAWSIoTConfiguration configuration;

        /**
         * Initialize Cloud Library
         * @param config. A JsonDocument containing keys named as CONFIG_KEY_* constants defined in Misc file
         * @param callback. A callback function to be called when a message is received from the MQTT Broker
         */
        void begin(const JsonDocument &config, MQTTClientCallbackAdvancedFunction callback);

        /**
         * Connect AWS IoT Core
         */
        void connect();

        /**
         * Check connection status
         * @return true if connected, false otherwise
         */ 
        bool connected();

        /**
         * Publish message to AWS IoT core
         * @param topic. Topic used to publish message
         * @param payload. Payload of message
         */
        bool publish(const char *topic, const char *payload);

        /**
         * Wrapper method for MQTT Loop
         */
        void loop();

        /**
         * Utility method used to create a complete topic starting from topic and TOPIC PREFIX
         * @param topic. Topic to be prefixed with TOPIC PREFIX
         * @param newTopic. Buffer containing the new TOPIC. Buffer size must be equal to TOPIC_SIZE
         */
        void completeTopic(const char *topic, char *newTopic);

        /**
         * Determine if a topic is a shadow topic
         * @param topic. Topic name
         * @return A value which belongs to ShadowTopics enums representing the shadow topic. If topic is not a shadow topic, NoShadowTopic is returned
         */
        ShadowTopics shadowTopicId(const char *topic);

        /**
         * Subscribe a topic.
         * @param topicToSubScribe. A topic to subscribe
         * @return true if topic can be subscribed. false otherwise
         */
        bool subscribe(const char *topic);
        
        /**
         * Subscribe to shadow topics.
         * @param topicsToSubScribe. An array of shadow topics to subscribe
         * @param numTopics. Number of topics to subscribe
         * @return true if all topics can be subscribed. false otherwise
         */
        bool subscribeShadows(ShadowTopics topicsToSubScribe[], int numTopics);

        /**
         * Retrieve classic shadow posting a message to topic $aws/things/THING_NAME/shadow/get
         */
        void getShadow();

        /**
         * Generate classic shadow topic from a ShadowTopicId and THING_NAME
         * @param topicId. A value which belongs to ShadowTopics enums representing the shadow topic
         * @param topic. Buffer containing the shadow topic. Buffer size must be equal to TOPIC_SIZE
         */
        void shadowTopicFromId(ShadowTopics topicId, char *topic);

        /**
         * Post a message to a specif shadow topic
         * @param topicId. A value which belongs to ShadowTopics enums representing the shadow topic
         * @param payload. Payload of message
         */
        bool updateShadow(ShadowTopics topicId, const char *payload);

        /**
         * Update classic shadow reporting connectivity status under {"state":{"reported":{"connected": "true"}}}
         */
        bool updateShadowConnectivityStatus();

        /**
         * Set MQTT Last Will and Testament. Topic used for last will is TOPIC_PREFIX + 'lastWill'
         * Last will payload is {"state":{"reported":{"connected": "false"}}}
         */
        void setWillAndTestament();
};


#endif
