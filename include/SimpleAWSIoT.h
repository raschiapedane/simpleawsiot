/*
SimpleAWSIoT Library. A simple library to connect to AWS IoT Core
Copyright (C) 2021  Andrea Prosperi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef __SIMPLEAWSIOT_H_
#define __SIMPLEAWSIOT_H_

#include <SimpleAWSIoTMisc.h>
#include <SimpleAWSIoTCore.h>
#include <SimpleAWSIoTShadow.h>

#endif

#ifndef SIMPLEAWSIOT_VERSION

#define SIMPLEAWSIOT_VERSION "0.9.1"
#define SIMPLEAWSIOT_VERSION_MAJOR 0
#define SIMPLEAWSIOT_VERSION_MINOR 9
#define SIMPLEAWSIOT_VERSION_REVISION 1

#endif
