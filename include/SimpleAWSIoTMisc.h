/*
SimpleAWSIoT Library. A simple library to connect to AWS IoT Core
Copyright (C) 2021  Andrea Prosperi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef __SIMPLEAWSIOT_MISC_H_
#define __SIMPLEAWSIOT_MISC_H_

#ifndef ARDUINOJSON_VERSION
#include <ArduinoJson.h>
#endif

#if defined(ESP8266)
#include <LittleFS.h>
#endif

#if defined(ESP32)
#include <SPIFFS.h>
#endif

#define CONFIG_FILE "/config.json" //Name of configuration file on LittleFS
#define CONFIG_SIZE 512 //Configuration file size to store in mem
#define CONFIG_VALUE_SIZE 128 //Size of keys used in JSON Documents

#define CONFIG_KEY_SID            "ssid"          //JSON KEY storing SSID of Wifi Network
#define CONFIG_KEY_PWD            "pwd"           //JSON KEY storing Password to connect to Wifi Network
#define CONFIG_KEY_AWSEP          "aws_ep"        //JSON KEY storing AWS IoT Core Endpoint
#define CONFIG_KEY_THINGNAME      "name"          //JSON KEY storing Name of thing as defined into AWS IoT Core
#define CONFIG_KEY_TOPIC_PREFIX   "topic_prefix"  //JSON KEY storing Name of thing as defined into AWS IoT Core

#define PAYLOAD_SIZE 512

/**
 * JSON KEY storing Thing identifier. This is just an internal identified which is used to read certifcate from littleFS
 * Certificates on LittleFS should be named {thingId}.crt and {thingId}.key
 */
#define CONFIG_KEY_THINGID        "id"      

/**
 * Read file content from LittleFS and stores it into the buffer
 * Please ensure that buffer is big enough to store file content
 * @param fileName Name of file to read
 * @param buffer Buffer storing file content
 */
void readFileContent(const char * fileName, char **buffer);

/**
 * Set clock using NTP. Fundamental for certificate validation
 */
time_t setClock();

/**
 * Read software configuration from file CONFIG_FILE stored into LittleFS
 * @param document. JSON Document containing configuration loaded from file
 */
void readStoredConfiguration(JsonDocument& document);

#endif