/*
SimpleAWSIoT Library. A simple library to connect to AWS IoT Core
Copyright (C) 2021  Andrea Prosperi

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef __SIMPLEAWSIOT_SHADOW_H_
#define __SIMPLEAWSIOT_SHADOW_H_

#define SHADOW_FW_KEY F("fw")
#define SHADOW_TS_KEY F("ts")
#define SHADOW_CONNECTED_KEY F("connected")
#define SHADOW_STATE_KEY F("state")
#define SHADOW_REPORTED_KEY "reported"
#define SHADOW_DESIRED_KEY "desired"

#endif