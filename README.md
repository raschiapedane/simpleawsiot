# SimpleAWSIoT

A simple connection client for AWS IoT Core. It supports Amazon generated certificates and classic shadow.

Supported Devices: ESP8266, ESP32

## Description

SimpleAWSIoT is a simple library to connect to AWS IoT. It supports AWS managed certificates and classic shadow.
SimpleAWSIoT requires certificates and configuration file on a filesystem image to be flashed into the microcontroller. 

To create a filesystem image using PlatformIO add relevant configuration files to data directory inside the PlatformIO project. Files required and their naming conventions are described in [Configuration](#markdown-header-configuration) and [AWS Certificates](#markdown-header-aws-certificates).

Once data directory is populated with Certificates and Configurations files, it's time to generate a filesystem image. To do that, select PlatformIO tab in Visual Studio Code, expand Project tasks/Platform and click Build Filesystem Image. To upload the filesystem image to the microcontroller, click "Upload Filesystem Image"

For additional information refer to [PlatformIO documentation](https://docs.platformio.org/en/latest/platforms/espressif32.html#uploading-files-to-file-system-spiffs)

## Configuration
SimpleAWSIoT requires an ArduinoJSON document to retrieve configuration information. This document should have the following key names:

* ssid - Name of WIFI Network
* pwd - Password of WIFI Network
* name - Thing name as defined in AWS IoT Core
* id - Thing Id. User defined unique identifier. Certificates file names are equal to this id
* aws_ep - AWS IoT Core Endpoint (ATS endpoint)
* topic_prefix - Common prefix that may be used to generate topics

It's possible to read a configuration file from the filesystem image from a file named /config.json using the following convenience function

```C
void readStoredConfiguration(JsonDocument& document);
```

This is an example of configuration file

```JSON
{
    "ssid":"MY_SID",
    "pwd":"MY_WIFI_PASSWORD",
    "name":"MY_THING_NAME",
    "id":"MY_THING_ID",
    "aws_ep":"MY_AWS_IOT_CORE_ENDPOINT",
    "topic_prefix": "MY_TOPIC_PREFIX"
}
```

## AWS Certificates

SimpleAWSIoT supports AWS managed certificates. These certificates are read from the filesystem image. Expected file names are:

* AmazonCA.pem
* {THING_ID_AS_SPECIFIED_IN_CONFIG_FILE}.pem
* {THING_ID_AS_SPECIFIED_IN_CONFIG_FILE}.key

These files contain respectively:

* Amazon Root CA
* Thing certificate as stored into AWS IoT Core
* Thing certificate private key as stored into AWS IoT Core

Refer to this guide to generate [AWS IoT Certificates](https://docs.aws.amazon.com/iot/latest/developerguide/device-certs-create.html). 


## Connection

Use the following code to initialize SimpleAWSIoT:

```C
//Initialize LittleFS
LittleFS.begin();

//Read Configuration from FLASH
DynamicJsonDocument configuration(CONFIG_SIZE);
readStoredConfiguration(configuration);

//Initialize Cloud, connect to AWS and set the mqttcallback function used when message is received from the cloud
simpleAWSIoT.begin(configuration, mqttcallback);
```

mttcallback is a callback function called when a message is received from IoT Core.
Callback definition is part of the [256dpi/MQTT](https://github.com/256dpi/arduino-mqtt) Library used by SimpleAWSIoT. It's defined as follow

```CPP
typedef void (*MQTTClientCallbackAdvanced)(MQTTClient *client, char topic[], char bytes[], int length);
```

## Last Will

SimpleAWSIoT supports Last Will. **It's important to configure Last Will before the connection using the following method**

```CPP
void setWillAndTestament();
```

Topic used for last will is the union of topic_prefix configuration variable, name configuration variable and "lastWill". So for example if you configured topic_prefix as "my_topics", name as 'my_thing' then last will topic will be "my_topics/my_thing/lastWill". YOU DON'T NEED ADDITIONAL TRAILING / for topic_prefix and thing name.

## Shadow

SimpleAWSIoT supports classic shadow of AWS IoT Core. It's possible to subscribe to these topics using the commodity method subscribeShadows.

```CPP
//Subscribe to interesting topics
ShadowTopics topics[] = {ShadowTopicUpdateDelta, ShadowTopicGetAccepted};
simpleAWSIoT.subscribeShadows(topics, 2); ///Second argument of subscribeShadows (in this case number 2) is the number of elements of the array
```

Messages received on these topics are sent to the MQTTCallback function defined during initialization of SimpleAWSIoT. You can determine the name of Shadow Topic message refer to using method.

```CPP
ShadowTopics shadowTopicId(const char *topic);
```

ShadowTopics is an enumeration type defined

```CPP
enum ShadowTopics {
  NoShadowTopic = -1,
  ShadowTopicGetAccepted = 0,
  ShadowTopicGetRejected = 1,
  ShadowTopicDeleteAccepted = 2,
  ShadowTopicDeleteRejected = 3,
  ShadowTopicUpdateAccepted = 4,
  ShadowTopicUpdateRejected = 5,
  ShadowTopicUpdateDelta = 6,
  ShadowTopicUpdateDocument = 7,
  ShadowTopicGet = 8,
  ShadowTopicDelete = 9,
  ShadowTopicUpdate = 10
} typedef ShadowTopics;
```
NoShadowTopic value refers to a topic that is not part of the reserved Shadow Topics of AWS IoT Core

## Updating connection status in shadow

SimpleAWSIoT provides a convenience method to update the shadow and report thing connectivity. 

```CPP
bool updateShadowConnectivityStatus();
```

Shadow will be updated using the following json document

```JSON
{
    "state": {
        "reported": {
            "connected": true
        }
    }
}
```
Last Will can be set to update connectivity status when the connection drops. Please refer to [AWS IoT Core - Developer Guide](https://docs.aws.amazon.com/iot/latest/developerguide/device-shadow-comms-app.html) for additional details.

## Publish

SimpleAWSIoT supports message publishing using publish method.

```CPP
bool publish(const char *topic, const char *payload);
```

SimpleAWSIoT provides a convenience method to generate topic using the following naming convention:

/topic_prefix/thing_name/topic where

topic_prefix - Value of topic_prefix key in configuration file
thing_name - Value of name key in configuration file
topic - Value passed to the method

```CPP
void completeTopic(const char *topic, char *newTopic);
```

newTopic size must be equal to TOPIC_SIZE constant. 


## Subscribe

SimpleAWSIoT supports topic subscribing using subscribe method.

```CPP
bool subscribe(const char *topic);
```

## Caveats

On ESP8266 payload size is limited to 512 bytes. Sending a bigger payload will force an MQTT disconnection and message is lost





